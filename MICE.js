// Melvor Idle Cheat Engine by aldousWatts on GitLab | Built for Melvor Idle alpha v0.16.3
// Hacking Melvor Idle for dummies! And learning/relearning Javascript along the way
// As always, use and modify at your own risk. But hey, contribute and share!
// This code is open source and shared freely under MPL/GNUv3/creative commons licenses.

// This content script serves three functions...
( () => {
    if (document.contains(document.getElementById('MICE-icon'))) return alert('MICE just tried to load, but detected that MICE was already on the page. This may mean that your browser has automatically updated the extension and you need to refresh to complete the update!');
    const isChrome = navigator.userAgent.match('Chrome');
    const getSrc = (file = 'icons/border-48.png') => (isChrome ? chrome : browser).runtime.getURL(file);

    // ...Icon injection...
    const miceIcon = document.createElement('img');
    miceIcon.src = getSrc();
    miceIcon.id = 'MICE-icon';
    miceIcon.className = 'd-none';
    document.body.append(miceIcon);

    // ...Version injection...
    /**
     * @param {string} version
     */
    const addMiceVersion = (version) => {
        const script = document.createElement('script');
        script.setAttribute('id', 'miceVersion');
        script.innerText = `const MICE_VERSION = '${version}';`
        document.body.prepend(script);
    };
    const miceVersion = (isChrome ? chrome : browser).runtime.getManifest().version;
    addMiceVersion(miceVersion);

    // ...and Script injection.
    const scriptID = 'inject-mice';
    if (document.contains(document.getElementById(scriptID))) document.getElementById(scriptID).remove();
    const script = document.createElement('script');
    script.src = getSrc('inject-mice.js');
    script.id = scriptID;
    document.body.appendChild(script);
})();
